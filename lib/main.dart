import 'package:flutter/material.dart';
import 'Screans/flamingo_screan.dart';
import 'Screans/home_screan.dart';

void main() =>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowMaterialGrid: false,
      debugShowCheckedModeBanner: false,
      home: Flamingo(),
    );
  }
}

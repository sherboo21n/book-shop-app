import 'package:book_shop/Models/category_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ContinueReading extends StatefulWidget {
  @override
  _ContinueReadingState createState() => _ContinueReadingState();
}

class _ContinueReadingState extends State<ContinueReading> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20,right: 20),
      child: Stack(
        children: <Widget>[
          Container(
            height: 85,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 15),
                    blurRadius: 27,
                    color: Colors.black12, // Black color with 12% opacity
                  )],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 22,
            child: Container(
              height: 7,
              width: 250,
              decoration: BoxDecoration(
                color: Colors.orange.shade900,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(
                      200
                  ),
                  bottomRight: Radius.circular(
                      50
                  ),
                  topRight: Radius.circular(
                    50
                  )
                )
              ),
            ),
          ),
          Positioned(
            top: 10,
            right: 20,
            child: Image.asset(
              category[0].image,
              fit: BoxFit.cover,
              height: 70,
            ),
          ),
          Positioned(
            top: 20,
            left: 30,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  category[0].title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  category[0].writer,
                  style: TextStyle(
                    color: Colors.grey
                  ),
                )
              ],
            ),
          ),
          Positioned(
            bottom: 10,
              right: 95,
            child: Text(
              'Chapter 5 of 50',
              style: TextStyle(
                fontSize: 10 ,
                color: Colors.grey
              ),
            ),
          )
        ],
      )
    );
  }
}


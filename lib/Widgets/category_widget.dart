import 'package:book_shop/Models/category_model.dart';
import 'package:book_shop/Screans/flamingo_screan.dart';
import 'package:book_shop/Screans/products_detaiels.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Categori extends StatefulWidget {
  @override
  _CategoriState createState() => _CategoriState();
}

class _CategoriState extends State<Categori> {

 int index;
  List ids = [];

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.only(
        top: 150,
        left: 20,
      ),
      child: Container(
          height: 270,
          child:  ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: category.length,
            itemBuilder: (context , index) =>  Container(
              width: 200,
              margin: EdgeInsets.symmetric(
                  horizontal: 5,
                  vertical: 10
              ),
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: <Widget>[
                    Container(
                      height: 220,
                      margin: EdgeInsets.only(
                          right: 10
                      ),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(
                              20
                          ),
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 0),
                              blurRadius: 2,
                              color: Colors.black12, // Black color with 12% opacity
                            )]
                      ),
                    ),
                    Positioned(
                      top: 0,
                      right: 30,
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20
                          ),
                          height: 150,
                          child: Image.asset(
                            category[index].image,
                            fit: BoxFit.cover,
                          ),
                        ),
                    ),
                    Positioned(
                      top: 50,
                      right: 20,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          IconButton(
                            icon: Icon(
                                ids.contains(index)? Icons.favorite : Icons.favorite_border
                            ),
                            onPressed:(){
                            if(
                            ids.contains(index)
                            ){
                              ids.remove(index);
                            }
                            else{
                              ids.add(index);
                            }
                            setState(() {});
                          },
                            color: ids.contains(index)? Colors.red : Colors.grey,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              right: 5,
                            ),
                            child: Container(
                              height: 55,
                              width: 35,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(30),
                                  boxShadow: [
                                    BoxShadow(
                                      offset: Offset(0, 15),
                                      blurRadius: 27,
                                      color: Colors.black12, // Black color with 12% opacity
                                    )]
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.star,
                                    color: Colors.orange,
                                  ),
                                  Text(
                                    category[index].rating.toString(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Positioned(
                      bottom: 45,
                      left: 20,
                      right: 20,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              category[index].title,
                            style: TextStyle(
                                fontWeight: FontWeight.bold
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                              category[index].writer,
                            style: TextStyle(
                              color: Colors.grey
                            ),
                          ),

                        ],
                      )
                    ),
                    Positioned(
                      bottom: 0,
                      left: 30,
                      child: Row(
                        children: <Widget>[
                          InkWell(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) =>ProductsDetailes(
                                image: category[index].image,
                                title: category[index].title,
                                title2: category[index].title2,
                                title3: category[index].title3,
                                writer: category[index].writer,
                                ratting: category[index].rating,
                              )));
                            },
                            child: Text(
                              'Details'
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: 15
                            ),
                              child: Container(
                                height: 40,
                                width: 100,
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(
                                        30
                                    ),
                                    bottomRight: Radius.circular(
                                        20
                                    ),
                                  )
                                ),
                                child: Center(
                                  child: InkWell(
                                    onTap: (){

                                    },
                                    child: Text(
                                      'Read',
                                      style: TextStyle(
                                        color: Colors.white
                                      ),
                                    ),
                                  ),
                                ),
                              ),

                          )
                        ],
                      ),
                    )
                  ],
                ),
            ),
          )
      ),
    );
  }
}


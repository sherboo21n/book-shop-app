import 'package:book_shop/Models/category_model.dart';
import 'package:flutter/material.dart';

class Best extends StatefulWidget {
  @override
  _BestState createState() => _BestState();
}

class _BestState extends State<Best> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 25,right: 25),
      child: Container(
        height: 200,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Container(
                  margin: EdgeInsets.only(
                      right: 10
                  ),
                  decoration: BoxDecoration(
                      color: Colors.grey.shade200,
                      borderRadius: BorderRadius.circular(
                          30
                      ),
                ),
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              child:  Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: 20
                  ),
                  height: 150,
                  child: Image.asset(
                    category[2].image,
                    fit: BoxFit.cover,
                  ),
                ),
            ),
            Positioned(
              bottom: 0,
              right: 10,
              child: Container(
                height: 40,
                width: 130,
                decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(
                          30
                      ),
                      bottomRight: Radius.circular(
                          20
                      ),
                    )
                ),
                child: Center(
                  child: InkWell(
                    onTap: (){

                    },
                    child: Text(
                      'Read',
                      style: TextStyle(
                          color: Colors.white
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 40,
              left: 20,
              right: 150,
              child:  Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'New York Time Best For March 2020',
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 10
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      category[2].title,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      category[2].writer,
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 16
                      ),
                    )
                  ],
                ),
            ),
            Positioned(
              bottom: 10,
              left: 20,
              right: 50,
              child:Row(
                children: <Widget>[
                  Container(
                    height: 55,
                    width: 35,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(
                            30
                        ),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(
                                0, 15
                            ),
                            blurRadius: 27,
                            color: Colors.black12, // Black color with 12% opacity
                          )]
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.star,
                          color: Colors.orange,
                        ),
                        Text(
                          category[2].rating.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Lorem ipsum dolor sit',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 10
                        ),
                      ),
                      Text(
                        ' amet, consectetur adipiscing elit, ',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 10
                        ),
                      ),
                      Text(
                        'Lorem ipsum dolor sit',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 10
                        ),
                      ),
                    ],
                  )
                ],
              )
            )
          ],
        ),
      )
    );
  }
}

import 'package:book_shop/Models/category_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'category_widget.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Stack(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 250,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(
                        70
                    ),
                  ),
                  image: DecorationImage(
                    image: AssetImage(
                      'images/main_page_bg.png',
                    ),
                    fit: BoxFit.cover,
                  )
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 80,
                  left: 35
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'What are you',
                    style: TextStyle(
                        fontSize: 24
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'reading',
                        style: TextStyle(
                            fontSize: 24
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        'today?',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 24
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
           Categori()
          ],
        )
      ],
    );
  }
}

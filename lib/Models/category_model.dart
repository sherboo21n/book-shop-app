class Category{
  final int id;
  final String image;
  final String title;
  final String title2;
  final String title3;
  final double rating;
  final String writer;

  Category({this.image,this.title,this.title2,this.title3,this.rating,this.writer,this.id});
}

List<Category> category = [
  Category(
    id: 1,
    image: 'images/book-1.png',
    title: 'Crushing & Influence',
    title2: 'Crushing &',
    title3: 'Influence',
    writer: 'Gray Venchuk',
    rating: 4.9,
  ),
  Category(
    id: 1,
    image: 'images/book-2.png',
    title: 'Top 10 Hacks...',
    title2: 'Top 10',
    title3: 'Hacks',
    writer: 'Herman Joel',
    rating: 5.6,
  ),
  Category(
    id: 1,
    image: 'images/book-3.png',
    title: 'How To Win Friends & Influence',
    title2: 'How To Win Friends & ',
    title3: 'Influence',
    writer: 'Gray Venchuk',
    rating: 3.8,
  ),
];
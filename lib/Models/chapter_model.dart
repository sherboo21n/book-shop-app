class Chapter{
  final String title;
  final String name;
  final String desc;

  Chapter({this.title,this.name,this.desc});

}

List<Chapter> chapter = [
  Chapter(
    title: 'Chapter 1:',
    name: 'Money',
    desc: 'Life is about change'
  ),
Chapter(
    title: 'Chapter 2:',
    name: 'Power',
    desc: 'Everything loves power'
  ),
Chapter(
    title: 'Chapter 3:',
    name: 'Influence',
    desc: 'Influence easily like never before'
  ),
Chapter(
    title: 'Chapter 4:',
    name: 'Win',
    desc: 'Winning ia what matters'
  ),
];
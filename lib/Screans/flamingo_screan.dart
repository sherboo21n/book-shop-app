import 'dart:collection';

import 'package:book_shop/Screans/home_screan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Flamingo extends StatefulWidget {
  @override
  _FlamingoState createState() => _FlamingoState();
}

class _FlamingoState extends State<Flamingo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        Image.asset(
          'images/Bitmap.png',
          fit: BoxFit.fill,
        width: MediaQuery.of(context).size.width,
        ),
         Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                      'flamin',
                      style: TextStyle(
                          fontSize: 32
                      )
                  ),
                  Text
                    ('go.',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                      fontSize: 32
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 15,
              ),
              RaisedButton(
                child: Padding(
                  padding: const EdgeInsets.only(left: 25,right: 25,bottom: 12,top: 12),
                  child: Text(
                    'Start reading',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 14
                  ),
                  ),
                ),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>HomeScrean()));
                },
                color: Colors.white,
                shape: StadiumBorder(),
              )
            ],
          ),


      ],),
    );
  }
}

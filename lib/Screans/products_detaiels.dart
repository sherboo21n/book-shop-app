import 'package:book_shop/Models/category_model.dart';
import 'package:book_shop/Models/chapter_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductsDetailes extends StatefulWidget {

  final String image , title , title2 , title3, writer ;
  final double ratting;
  ProductsDetailes({this.writer,this.title,this.title2,this.title3,this.image,this.ratting});

  @override
  _ProductsDetailesState createState() => _ProductsDetailesState();
}

class _ProductsDetailesState extends State<ProductsDetailes> {

  int index;
  List ids = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
             Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 350,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(
                              70
                          ),
                          bottomRight: Radius.circular(
                              70
                          ),
                        ),
                        image: DecorationImage(
                          image: AssetImage(
                            'images/bg.png'
                          ),
                          fit: BoxFit.cover,
                        )
                    ),
                  ),
                  Positioned(
                    top: 50,
                    right: 0,
                    child: Image.asset(
                      widget.image,
                      fit: BoxFit.cover,
                      height: 200,
                    ),
                  ),
                  Positioned(
                    top: 100,
                    left: 30,
                    right: 140,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          widget.title2,
                          style: TextStyle(
                            fontSize: 28,
                            color: Colors.grey.shade800
                          ),
                        ),
                        Text(
                          widget.title3,
                          style: TextStyle(
                            fontSize: 28,
                            fontWeight: FontWeight.bold
                          ),
                        )
                      ],
                    )
                  ),
                  Positioned(
                    top: 180,
                    left: 30,
                    right: 140,
                    child: Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Lorem ipsum dolor sit',
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 10
                              ),
                            ),
                            Text(
                              ' amet, consectetur adipiscing elit, ',
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 10
                              ),
                            ),
                            Text(
                              ' amet, consectetur adipiscing elit, ',
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 10
                              ),
                            ),
                            Text(
                              'Lorem ipsum dolor sit',
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 10
                              ),
                            ),
                            Text(
                              'Lorem ipsum ',
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 10
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            RaisedButton(
                              shape: StadiumBorder(),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  left: 30,
                                  right: 30
                                ),
                                child: Text(
                                  'Read'
                                ),
                              ),
                              onPressed: (){

                              },
                              color: Colors.white,
                            )
                          ],
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Column(
                          children: <Widget>[
                            IconButton(
                              icon: Icon(
                                  ids.contains(index)? Icons.favorite : Icons.favorite_border
                              ),
                              onPressed:(){
                                if(
                                ids.contains(index)
                                ){
                                  ids.remove(index);
                                }
                                else{
                                  ids.add(index);
                                }
                                setState(() {});
                              },
                              color: ids.contains(index)? Colors.red : Colors.grey,
                            ),
                            Container(
                              height: 55,
                              width: 30,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(30),
                                  boxShadow: [
                                    BoxShadow(
                                      offset: Offset(0, 15),
                                      blurRadius: 27,
                                      color: Colors.black12, // Black color with 12% opacity
                                    )]
                              ),
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      color: Colors.orange,
                                    ),
                                    Text(
                                      widget.ratting.toString(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )

                      ],
                    )
                  ),
                  Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 300,
                          left: 20,
                        ),
                        child: Container(
                            height: 300,
                            child:  ListView.builder(
                              scrollDirection: Axis.vertical,
                              itemCount: chapter.length,
                              itemBuilder: (context , index) =>  Container(
                                width: 200,
                                margin: EdgeInsets.symmetric(
                                    horizontal: 5,
                                  vertical: 5
                                ),
                                child: Stack(
                                  alignment: Alignment.bottomCenter,
                                  children: <Widget>[
                                    Container(
                                      height: 60,
                                      margin: EdgeInsets.only(
                                          right: 10
                                      ),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(
                                              50
                                          ),
                                          boxShadow: [
                                            BoxShadow(
                                              offset: Offset(0, 15),
                                              blurRadius: 5,
                                              color: Colors.black12, // Black color with 12% opacity
                                            )]
                                      ),
                                    ),
                                    Positioned(
                                      top: 25,
                                      right: 40,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                      ),
                                    ),
                                    Positioned(
                                      top: 10,
                                      left: 40,
                                      bottom: 10,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                         Row(
                                           children: <Widget>[
                                             Text(
                                               chapter[index].title,
                                               style: TextStyle(
                                                 fontWeight: FontWeight.bold
                                               ),
                                             ) ,
                                             Text(
                                               chapter[index].name,
                                               style: TextStyle(
                                                 fontWeight: FontWeight.bold
                                               ),
                                             )
                                           ],
                                         ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            chapter[index].desc,
                                            style: TextStyle(
                                              color: Colors.grey
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                        ),
                      )

                    ],
                  )
                ],
              ),
          Padding(
              padding: EdgeInsets.only(
                  left: 40
              ),
              child: Row(
                children: <Widget>[
                  Text(
                    "You might also",
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 18
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                      'like...',
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold
                      )
                  )
                ],
              )
          ),
          SizedBox(
            height: 30,
          ),
         Stack(
           children: <Widget>[
            Container(
                 height: 200,
                 decoration: BoxDecoration(
                   color: Colors.white,
                 ),
               ),
             Positioned(
               top: 0,
               right: 20,
               child: Image.asset(
                 category[2].image,
                 fit: BoxFit.cover,
               ),
             ),
             Positioned(
               top: 0,
               left: 40,
               right: 195,
               child: Column(
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: <Widget>[
                   Text(
                     category[2].title,
                     style: TextStyle(
                       fontSize: 20,
                       fontWeight: FontWeight.bold
                     ),
                   ),
                   SizedBox(
                     height: 5,
                   ),
                   Text(
                     category[2].writer,
                     style: TextStyle(
                       color: Colors.grey
                     ),
                   )
                 ],
               ),
             ),
             Positioned(
               bottom: 55,
               left: 40,
               child: Container(
                 height: 70,
                 width: 40,
                 decoration: BoxDecoration(
                     color: Colors.white,
                     borderRadius: BorderRadius.circular(30),
                     boxShadow: [
                       BoxShadow(
                         offset: Offset(0, 15),
                         blurRadius: 27,
                         color: Colors.black12, // Black color with 12% opacity
                       )]
                 ),
                 child: Column(
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: <Widget>[
                     Icon(
                       Icons.star,
                       color: Colors.orange,
                     ),
                     Text(
                       category[2].rating.toString(),
                       style: TextStyle(
                           fontWeight: FontWeight.bold
                       ),
                     )
                   ],
                 ),
               ),
             ),
             Positioned(
               bottom: 55,
               left: 120,
               child: RaisedButton(
                 shape: StadiumBorder(),
                 child: Padding(
                   padding: const EdgeInsets.only(
                       left: 30,
                       right: 30
                   ),
                   child: Text(
                       'Read'
                   ),
                 ),
                 onPressed: (){

                 },
                 color: Colors.white,
               ),
             )
           ],
         ),
          SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }
}

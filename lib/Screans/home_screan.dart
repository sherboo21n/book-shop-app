import 'package:book_shop/Models/category_model.dart';
import 'package:book_shop/Widgets/best_of_the_day_widget.dart';
import 'package:book_shop/Widgets/continue_reading_widget.dart';
import 'package:book_shop/Widgets/home_screan_header_widget.dart';
import 'package:flutter/material.dart';

class HomeScrean extends StatefulWidget {
  @override
  _HomeScreanState createState() => _HomeScreanState();
}

class _HomeScreanState extends State<HomeScrean> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:ListView(
        shrinkWrap: true,
        children: <Widget>[
        Header(),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: EdgeInsets.only(
                left: 35
            ),
            child: Row(
              children: <Widget>[
                Text(
                    "Best of the",
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 18
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  'day',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold
                  )
                )
              ],
            )
          ),
          Best(),
          SizedBox(
            height: 20,
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 35
              ),
              child: Row(
                children: <Widget>[
                  Text(
                    "Continue",
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 18
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                      'reading...',
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold
                      )
                  )
                ],
              )
          ),
          SizedBox(
            height: 20,
          ),
          ContinueReading(),
          SizedBox(
            height: 20,
          ),
        ],
      )
    );
  }
}
